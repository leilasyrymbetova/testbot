<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommandLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('command_langs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('command_id');
            $table->integer('language_id');
            $table->string('command_text');

            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('command_id')->references('id')->on('commands');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('command_langs');
    }
}
