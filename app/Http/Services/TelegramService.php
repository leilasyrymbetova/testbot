<?php

namespace App\Http\Services;

use App\Http\Controllers\TelegramController;
use Illuminate\Database\Eloquent\Model;
use App\Models\Command;
use App\Models\Command_lang;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use function Webmozart\Assert\Tests\StaticAnalysis\string;

class TelegramService
{
    protected $account;
    protected $command;
    protected $subcommand;
    const URL = 'https://api.telegram.org/bot';

    public function justDoIt($result)
    {
        $this->account = User::firstOrCreate(
            ['name' =>  $result['message']['chat']['first_name'],
            'surname' =>  $result['message']['chat']['username'],
            'telegram_id' => $result['message']['chat']['id'],
            'language_id' => 2]);

        $command = $result['message']['text'];
        $this->showAction($command);
    }

    public function sendCommands(){
        $this->subcommand = Command::where('parent_id', $this->command->id)->first();
        $commandLang = Command_lang::where('command_id', $this->subcommand->id)
            ->where('language_id', $this->account->language_id)->first();
        $this->command = $this->subcommand;
        $this->sendMessage($this->account->telegram_id, $commandLang->command_text);
    }

    public function showAction($com)
    {
        if (Command::where('command_name', $com)->first()) {
            $this->command = Command::where('command_name', $com)->first();
            $this->sendCommands();
        } else {
            $this->sendCommands();
        }

    }

    public function sendMessage($chat_id, $text)
    {
        Http::post(self::URL.config('bots.bot').'/sendMessage', [
            'chat_id' => $chat_id,
            'text' => $text
        ]);
    }

//    public function sendButtons($commandLang)
//    {
//            $buttons = [
//                'inline_keyboard' => [
//                    [
//                        [
//                            'text' => $commandLang,
//                            'callback_data' => '1'
//                        ],
//                    ],
//
//                ]
//            ];
//
//        return Http::post(self::URL.config('bots.bot').'/sendMessage', [
//            'chat_id' => $this->account->telegram_id,
//            'text' => 'Select the button',
//            'parse_mode' => 'html',
//            'reply_markup' => $buttons
//        ]);
//    }



}
