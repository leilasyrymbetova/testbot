<?php

namespace App\Http\Controllers;

use App\Helpers\Telegram;
use App\Http\Services\TelegramService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class TelegramController extends Controller
{
    protected $telegramService;
    protected $http;
    protected $config;
    const URL = 'https://api.telegram.org/bot';

    public function __construct(TelegramService $telegramService)
    {
        $this->telegramService = $telegramService;
    }


    public function sendAPIRequest($url, array $content, $post = true)
    {
        if (isset($content['chat_id'])) {
            $url = $url.'?chat_id='.$content['chat_id'];
            unset($content['chat_id']);
        }

        if ($post){
            return Http::post($url, $content);
        } else{
            return Http::get($url);
        }

    }

    //конкатенация урл
    public function endpoint($api, array $content, $post = true)
    {
        $url = self::URL.'5789437626:AAFu76VvAEK8ClA1L68qLBgqWFdpr8Eo8uU'.'/'.$api;
        if ($post) {
            $reply = $this->sendAPIRequest($url, $content);
        } else {
            $reply = $this->sendAPIRequest($url, [], false);
        }

        return json_decode($reply, true);
    }


    public function getUpdates($offset = 0, $limit = 100, $timeout = 0, $update = true)
    {
        $content = ['offset' => $offset, 'limit' => $limit, 'timeout' => $timeout];
        $updates = $this->endpoint('getUpdates', $content);
        if ($update) {
            if (array_key_exists('result', $updates) && is_array($updates['result']) && count($updates['result']) >= 1) {
                $last_element_id = $updates['result'][count($updates['result']) - 1]['update_id'] + 1;
                $content = ['offset' => $last_element_id, 'limit' => '1', 'timeout' => $timeout];
                $this->endpoint('getUpdates', $content); //больше не показывай те, которые просмотрела
            }
        }

        return $updates;
    }

    public function index(Request $request)
    {
        while (true) {
            $updateData = $this->getUpdates();
//            Log::debug(json_encode($updateData));
            foreach ($updateData['result'] as $result){
                $this->telegramService->justDoIt($result);
            }
            sleep(1);
        }

    }


}
