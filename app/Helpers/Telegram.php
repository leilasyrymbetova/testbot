<?php

namespace App\Helpers;

use App\Models\Account;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Psy\Context;


class Telegram
{
    protected $http;
    protected $config;
    const URL = 'https://api.telegram.org/bot';


    public function __construct(Http $http)
    {
        $this->http = $http;
        $this->setConfig();
//        dd($this->config['bot']);
    }

    public function setConfig()
    {
        $this->config = config('bots');
    }

    public function sendMessage($chat_id, $message)
    {
//        dd($this->config['bot']);
//        $this->setConfig();
        return $this->http::post(self::URL.$this->config['bot'].'/sendMessage', [
            'chat_id' => $chat_id,
            'text' => $message,
            'parse_mode' => 'html'
        ]);
    }

    public function setHook(){
        $url = 'https://webhook.site/2d5153b6-5cb3-4c5a-8d05-7c680b17924d';
        $data = [
            'status_code' => 200,
            'status' => 'success',
            'message' => 'webhook send successfully',
            'extra_data' => [
                'first_name' => 'Harsukh',
                'last_name' => 'Makwana',
            ],
        ];
        $json_array = json_encode($data);
        $curl = curl_init();
        $headers = ['Content-Type: application/json'];

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $json_array);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $response = curl_exec($curl);
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($http_code >= 200 && $http_code < 300) {
            echo "webhook send successfully.";
        } else {
            echo "webhook failed.";
        }
    }


    public function sendDocument($chat_id, $file, $reply_id=null)
    {
        return $this->http::attach('document', file_get_contents($file), 'sample.pdf')
            ->post(self::URL.$this->config['bot'].'/sendDocument', [
            'chat_id' => $chat_id,
                 'reply_to_message_id'=>$reply_id
        ]);
    }

    public function sendPhoto($chat_id, $photo)
    {
        return $this->http::attach('photo', file_get_contents($photo), 'tyshkan.jpg')
            ->post(self::URL.$this->config['bot'].'/sendPhoto', [
                'chat_id' => $chat_id,
            ]);
    }

    public function sendButtons($chat_id, $message, $buttons)
    {
        return $this->http::post(self::URL.$this->config['bot'].'/sendMessage', [
            'chat_id' => $chat_id,
            'text' => $message,
            'parse_mode' => 'html',
            'reply_markup' => $buttons
        ]);
    }


}

