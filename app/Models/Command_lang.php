<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Command_lang extends Model
{
    use HasFactory;

    protected $fillable = [
        'command_id',
        'language_id',
        'command_text'
    ];
}
