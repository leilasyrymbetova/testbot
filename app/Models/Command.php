<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Command extends Model
{
    use HasFactory;

    protected $fillable = [
        'command_name',
        'parent_id'
    ];

    public function languaging()
    {
        return $this->hasMany(Command_lang::class, 'command_id');
    }
}
