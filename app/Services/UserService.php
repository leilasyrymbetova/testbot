<?php

namespace App\Services;

use App\Models\User;
use App\Helpers;
use Illuminate\Database\Eloquent\Model\Account;

class UserService
{
    public function registerUser(int $telegram_id): Account{
        return Account::firstOrCreate([
            'telegram_id'=>$telegram_id
        ]);
    }
}

