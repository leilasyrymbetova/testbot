<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;
use \Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::get('/sendmessage', function(){
//   $response = Http::post('https://api.telegram.org/bot5789437626:AAFu76VvAEK8ClA1L68qLBgqWFdpr8Eo8uU/sendMessage',[
//       'chat_id'=>528608442,
//       'text'=>'jdscns',
//       'parse_mode' => 'html']);
//   \Illuminate\Support\Facades\Log::debug($response->status());
//   \Illuminate\Support\Facades\Log::debug($response->body());
//});

//Route::get('/senddocument', function (){
//    $response = Http::post('https://api.telegram.org/bot5789437626:AAFu76VvAEK8ClA1L68qLBgqWFdpr8Eo8uU/sendDocument',[
//        'chat_id'=>528608442,
//        'document'=>'http://www.africau.edu/images/default/sample.pdf']);
//    \Illuminate\Support\Facades\Log::debug($response->status());
//    \Illuminate\Support\Facades\Log::debug($response->body());
//});

//Route::get('/sendphoto', function (){
//    $response = Http::post('https://api.telegram.org/bot5789437626:AAFu76VvAEK8ClA1L68qLBgqWFdpr8Eo8uU/sendPhoto',[
//        'chat_id'=>528608442,
//        'photo'=>'https://ltdfoto.ru/image/e850Vi']);
//    \Illuminate\Support\Facades\Log::debug($response->status());
//    \Illuminate\Support\Facades\Log::debug($response->body());
//});

//Route::get('/sendbuttons', function(){
//    $buttons = [
//        'inline_keyboard'=>[
//            [
//                [
//                    'text'=>'button1',
//                    'callback_data'=> '1'
//                ],
//            ],
//            [
//                [
//                    'text'=>'button2',
//                    'callback_data'=> '2'
//                ],
//            ],
//        ]
//    ];
//    $response = Http::post('https://api.telegram.org/bot5789437626:AAFu76VvAEK8ClA1L68qLBgqWFdpr8Eo8uU/sendMessage',[
//        'chat_id'=>528608442,
//        'text'=>'select the button please',
//        'parse_mode' => 'html',
//        'reply_markup'=>json_encode($buttons)]);
//    \Illuminate\Support\Facades\Log::debug($response->status());
//    \Illuminate\Support\Facades\Log::debug($response->body());
//});

Route::get('/editbuttons', function(){
    $buttons = [
        'inline_keyboard'=>[
            [
                [
                    'text'=>'button1',
                    'callback_data'=> '1'
                ],
            ],
            [
                [
                    'text'=>'button2',
                    'callback_data'=> '2'
                ],
            ],
        ]
    ];
    $response = Http::post('https://api.telegram.org/bot5789437626:AAFu76VvAEK8ClA1L68qLBgqWFdpr8Eo8uU/editMessageText',[
        'chat_id'=>528608442,
        'text'=>'jdscns',
        'parse_mode' => 'html',
        'reply_markup'=>json_encode($buttons),
        'message_id'=>66,
        ]);
    \Illuminate\Support\Facades\Log::debug($response->status());
    \Illuminate\Support\Facades\Log::debug($response->body());
});


Route::get('/bot', function(\Illuminate\Http\Request $request){
    \Illuminate\Support\Facades\Log::debug("ddd");
   \Illuminate\Support\Facades\Log::debug($request->all());
});


Route::post('/bot', function(\Illuminate\Http\Request $request){
    \Illuminate\Support\Facades\Log::debug($request->all());
});

Route::get('/here', \App\Http\Controllers\TelegramController::class.'@index');
